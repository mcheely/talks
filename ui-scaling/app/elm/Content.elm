module Content exposing (slides)

import Slides exposing (centered)
import Html exposing (text)
import Counter


slides =
    [ centered (text "Hi, I'm Matt")
    , centered (text "Let me tell  you about these slides")
    , centered (text "#1")
    , centered (text "The Larch")
    , centered (text "The Larch")
    , centered (text "#6")
    ]
