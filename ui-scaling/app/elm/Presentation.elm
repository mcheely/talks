module Presentation exposing (Presentation, subscriptions, update, create, view, Msg, Slide, asSlide)

import Html exposing (Html, text, div)
import Html.Attributes exposing (style)
import Html.Events exposing (on)
import Keyboard exposing (KeyCode)
import Json.Decode as Decode


type alias Program model msg =
    { init : ( model, Cmd msg )
    , update : msg -> model -> ( model, Cmd msg )
    , view : model -> Html msg
    , subscriptions : model -> Sub msg
    }


type Slide
    = Static (Html Never)


toHtml : Slide -> Html Msg
toHtml slide =
    case slide of
        Static html ->
            Html.map (always NoOp) html


asSlide : Html Never -> Slide
asSlide html =
    Static html


type Presentation
    = Displaying (List Slide) Slide (List Slide)
    | TransitionForward (List Slide) Slide Slide (List Slide)
    | TransitionBack (List Slide) Slide Slide (List Slide)


create : List Slide -> Presentation
create slides =
    case slides of
        first :: rest ->
            Displaying [] first rest

        [] ->
            Displaying [] (Static (div [] [])) []



-- Update


type Msg
    = Previous
    | Next
    | AnimationFinished
    | NoOp


update : Msg -> Presentation -> Presentation
update msg presentation =
    case msg of
        Next ->
            advance presentation

        Previous ->
            regress presentation

        AnimationFinished ->
            completeTransition presentation

        NoOp ->
            presentation


advance : Presentation -> Presentation
advance presentation =
    case presentation of
        Displaying prev active remaining ->
            case remaining of
                next :: future ->
                    TransitionForward prev active next future

                [] ->
                    presentation

        TransitionBack prev entering exiting remaining ->
            TransitionForward prev entering exiting remaining

        _ ->
            presentation


regress : Presentation -> Presentation
regress presentation =
    case presentation of
        Displaying prev active remaining ->
            case prev of
                [] ->
                    presentation

                last :: previous ->
                    TransitionBack previous last active remaining

        TransitionForward prev exiting entering remaining ->
            TransitionBack prev exiting entering remaining

        _ ->
            presentation


completeTransition : Presentation -> Presentation
completeTransition presentation =
    case presentation of
        TransitionForward prev exiting entering future ->
            Displaying (exiting :: prev) entering future

        TransitionBack prev entering exiting future ->
            Displaying prev entering (exiting :: future)

        _ ->
            presentation



-- View


view : Presentation -> Html Msg
view model =
    div
        [ style
            [ ( "display", "flex" )
            , ( "overflow-x", "hidden" )
            ]
        ]
        (showPresentation model)


showPresentation : Presentation -> List (Html Msg)
showPresentation presentation =
    case presentation of
        Displaying previous active remaining ->
            (List.map hiddenSlide previous) ++ (List.map visibleSlide (active :: remaining))

        TransitionForward previous exiting entering remaining ->
            (List.map hiddenSlide previous) ++ [ leavingLeft exiting ] ++ (List.map visibleSlide (entering :: remaining))

        TransitionBack previous entering exiting remaining ->
            (List.map hiddenSlide previous) ++ [ returningLeft entering ] ++ (List.map visibleSlide (exiting :: remaining))


visibleSlide : Slide -> Html Msg
visibleSlide slide =
    div
        [ slideStyle [] ]
        [ toHtml slide ]


hiddenSlide : Slide -> Html Msg
hiddenSlide slide =
    div
        [ slideStyle
            [ ( "margin-left", "-100vw" )
            ]
        ]
        [ toHtml slide ]


transition : ( String, String )
transition =
    ( "transition", "0.5s" )


leavingLeft : Slide -> Html Msg
leavingLeft slide =
    div
        [ slideStyle
            [ ( "margin-left", "-100vw" )
            , transition
            ]
        , on "transitionend" (Decode.succeed AnimationFinished)
        ]
        [ toHtml slide ]


returningLeft : Slide -> Html Msg
returningLeft slide =
    div
        [ slideStyle
            [ ( "margin-left", "0" )
            , transition
            ]
        , on "transitionend" (Decode.succeed AnimationFinished)
        ]
        [ toHtml slide ]


slideStyle : List ( String, String ) -> Html.Attribute Msg
slideStyle attrs =
    style
        ([ ( "width", "100vw" )
         , ( "height", "100vh" )
         , ( "flex-shrink", "0" )
         ]
            ++ attrs
        )



-- Subscriptions


subscriptions : Sub Msg
subscriptions =
    Keyboard.presses mapKeys


mapKeys : KeyCode -> Msg
mapKeys code =
    case code of
        37 ->
            Previous

        39 ->
            Next

        _ ->
            NoOp
