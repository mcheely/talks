module Counter exposing (Model, Msg, init, update, view, subscriptions)

import Html exposing (div, button, text)
import Html.Events exposing (onClick)


type alias Model =
    Int


program =
    { init = init
    , update = update
    , view = view
    , subscriptions = subscriptions
    }


init =
    ( 0, Cmd.none )


type Msg
    = Increment


update msg model =
    case msg of
        Increment ->
            ( model + 1, Cmd.none )


view model =
    div []
        [ div [] [ text (toString model) ]
        , div [] [ button [ onClick Increment ] [ text "+1" ] ]
        ]


subscriptions model =
    Sub.none
