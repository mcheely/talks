module Slides exposing (..)

import Presentation exposing (Slide, asSlide)
import Html exposing (div, Html, text)
import Html.Attributes exposing (style)


centered : Html Never -> Slide
centered html =
    div
        [ style
            [ ( "text-align", "center" )
            , ( "font-size", "20px" )
            , ( "display", "flex" )
            , ( "height", "100%" )
            , ( "align-items", "center" )
            , ( "justify-content", "center" )
            ]
        ]
        [ html ]
        |> asSlide
