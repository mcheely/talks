module Main exposing (main)

import Html exposing (Html)
import Presentation exposing (Presentation)
import Content exposing (slides)
import Counter


main : Program Never Model Msg
main =
    Html.program
        { init = init
        , update = update
        , view = view
        , subscriptions = \_ -> Sub.map PresentAction Presentation.subscriptions
        }


type alias Model =
    { counterState : Counter.Model
    , presentationState : Presentation
    }


init : ( Model, Cmd msg )
init =
    let
        ( counterState, counterCmd ) =
            Counter.init
    in
        ( { counterState = counterState
          , presentationState = (Presentation.create Content.slides)
          }
        , counterCmd
        )


type Msg
    = PresentAction Presentation.Msg


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        PresentAction presentMsg ->
            ( { model
                | presentationState = Presentation.update presentMsg model.presentationState
              }
            , Cmd.none
            )


view : Model -> Html Msg
view model =
    Html.map PresentAction (Presentation.view model.presentationState)
