module GifSearch exposing (..)

import Html exposing (Html, text, button, div, img, input, form)
import Html.Attributes exposing (src, class, defaultValue)
import Html.Events exposing (onSubmit, onInput)
import Http
import Json.Decode as Decode exposing (Decoder)


main =
    Html.program
        { init = init
        , update = update
        , view = view
        , subscriptions = subs
        }



-- Model


type alias Model =
    { imgUrl : String
    , searchTerm : String
    }


init : ( Model, Cmd Msg )
init =
    ( { imgUrl = notFoundImg
      , searchTerm = ""
      }
    , Cmd.none
    )


giphyKey : String
giphyKey =
    "vSKgXSeeaqqZZMy4huWtGhiJfygTmtpC"


notFoundImg : String
notFoundImg =
    "https://media.giphy.com/media/26xBIygOcC3bAWg3S/giphy.gif"


searchDecoder : Decoder String
searchDecoder =
    Decode.at [ "data", "image_url" ] Decode.string



-- Update


type Msg
    = DoSearch
    | NewSearchTerm String
    | SearchResults (Result Http.Error String)


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        NewSearchTerm newTerm ->
            if newTerm == "cat" then
                ( { model | searchTerm = "opossum" }, search "opossum" )
            else
                ( { model | searchTerm = newTerm }, Cmd.none )

        DoSearch ->
            ( model, search model.searchTerm )

        SearchResults result ->
            let
                url =
                    Result.withDefault notFoundImg result
            in
                ( { model | imgUrl = url }, Cmd.none )


search : String -> Cmd Msg
search tag =
    Http.get ("https://api.giphy.com/v1/gifs/random?tag=" ++ tag ++ "&rating=g&api_key=" ++ giphyKey)
        searchDecoder
        |> Http.send SearchResults



-- View


view : Model -> Html Msg
view model =
    div [ class "cat-picker" ]
        [ div [ class "cat-image" ]
            [ img [ src model.imgUrl ] [] ]
        , form [ onSubmit DoSearch ]
            [ input
                [ defaultValue model.searchTerm
                , onInput NewSearchTerm
                ]
                []
            , button [] [ text "Search" ]
            ]
        ]



-- Subscriptions


subs : Model -> Sub Msg
subs model =
    Sub.none
