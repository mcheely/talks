module SimpleProgram exposing (..)

import Html exposing (beginnerProgram, Html, div, h1, text, button)
import Html.Attributes exposing (class)
import Html.Events exposing (onClick)


main =
    Html.beginnerProgram
        { model = initialState
        , update = update
        , view = view
        }


type alias Model =
    { count : Int }


initialState : Model
initialState =
    { count = 0 }


type Msg
    = Increment
    | Decrement


update : Msg -> Model -> Model
update msg model =
    case msg of
        Increment ->
            { model | count = (model.count + 1) }

        Decrement ->
            { model | count = (model.count - 1) }


view : Model -> Html Msg
view model =
    div [ class "wrapper" ]
        [ h1 [ class "count" ]
            [ text (toString model.count) ]
        , div [ class "controls" ]
            [ button [ onClick Increment ]
                [ text "Bigger!" ]
            , button [ onClick Decrement ]
                [ text "Smaller!" ]
            ]
        ]
