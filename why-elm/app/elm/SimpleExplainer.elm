module SimpleExplainer exposing (..)

import SimpleProgram
import Html exposing (Html, text, div)
import Html.Attributes exposing (class)
import Svg exposing (Svg, svg, ellipse, path, animateMotion, circle, mpath, g, animate, rect, marker)
import Svg.Events exposing (onEnd)
import Svg.Attributes
    exposing
        ( fill
        , stroke
        , strokeWidth
        , opacity
        , cx
        , cy
        , rx
        , ry
        , width
        , height
        , viewBox
        , preserveAspectRatio
        , textAnchor
        , x
        , y
        , dy
        , d
        , fillOpacity
        , xlinkHref
        , dur
        , r
        , id
        , begin
        , min
        , from
        , to
        , attributeName
        , refX
        , refY
        , markerWidth
        , markerHeight
        , markerEnd
        , orient
        )


main =
    Html.beginnerProgram
        { model = initialState
        , update = update
        , view = view
        }


type alias Model =
    { simpleProgramState : SimpleProgram.Model
    , displayedState : SimpleProgram.Model
    , pendingMessages : List PendingState
    , nextId : Int
    }


type alias PendingState =
    { order : Int
    , msg : SimpleProgram.Msg
    , model : SimpleProgram.Model
    }


initialState : Model
initialState =
    let
        subState =
            SimpleProgram.initialState
    in
        { simpleProgramState = subState
        , displayedState = subState
        , pendingMessages = []
        , nextId = 0
        }



-- Update


type Msg
    = ProgMsg SimpleProgram.Msg
    | UpdateModelDisplay SimpleProgram.Model
    | AnimateComplete PendingState


update : Msg -> Model -> Model
update msg model =
    case msg of
        UpdateModelDisplay newState ->
            { model | displayedState = newState }

        AnimateComplete pendingState ->
            { model
                | simpleProgramState = pendingState.model
                , pendingMessages = List.filter (((==) pendingState) >> not) model.pendingMessages
            }

        ProgMsg progMsg ->
            let
                nextState =
                    SimpleProgram.update progMsg model.simpleProgramState
            in
                { model
                    | pendingMessages = model.pendingMessages ++ [ PendingState model.nextId progMsg nextState ]
                    , nextId = model.nextId + 1
                }


view : Model -> Html Msg
view model =
    div [ class "explainer" ]
        [ div [ class "elm-architecture" ]
            ([ svg [ width "100%", height "100%", viewBox "0 0 1000 1200", preserveAspectRatio "xMidYMid" ]
                [ arrowHead
                , section "update" 225 475 "#DDD"
                , section "model" 525 775 "#DDD"
                , section "view" 825 1175 "#DDD"
                , oval ( 500, 350 )
                , oval ( 500, 650 )
                , viewOval ( 500, 1000 )
                , curve "modelUpdateConnector" modelUpdatePath
                , curve "viewUpdateConnector" viewUpdatePath
                , curve "modelViewConnector" modelViewPath
                , curve "updateModelPath" updateModelPath
                , invisible (curve "updateInternalPath" updateInternalPath) []
                , invisible (curve "modelSetPath" modelSetPath) []
                , invisible (curve "modelUpdateAnimationPath" modelUpdateAnimationPath) []
                , modelBlob ( 500, 650 ) 50 "currentState" model.displayedState
                , invisible (curve "modelRenderPath" modelRenderPath) []
                ]
             ]
                ++ List.map (animatedMessage model.displayedState) model.pendingMessages
            )
        , div [ class "app-embed" ]
            [ Html.map ProgMsg <| SimpleProgram.view model.simpleProgramState
            ]
        ]


animatedMessage : SimpleProgram.Model -> PendingState -> Html Msg
animatedMessage currentState pendingState =
    let
        order =
            pendingState.order

        msg =
            pendingState.msg

        model =
            pendingState.model

        msgBlobId =
            "msgBlob" ++ (toString order)

        modelBlobId =
            "modelBlob" ++ (toString order)

        newModelId =
            "newModel" ++ (toString order)

        msgColor =
            case msg of
                SimpleProgram.Increment ->
                    "tomato"

                SimpleProgram.Decrement ->
                    "blue"
    in
        svg [ width "100%", height "100%", viewBox "0 0 1000 1200", preserveAspectRatio "xMidYMid" ]
            [ modelBlob ( 0, 0 ) 50 modelBlobId currentState
            , invisible (modelBlob ( 0, 0 ) 50 "" model) [ id newModelId ]
            , msgBlob msgColor msgBlobId
            , animateBlobPath "viewUpdateConnector" msgBlobId [ begin "0s", dur "1.5s", id (msgBlobId ++ "a1") ]
            , animateBlobPath "modelUpdateAnimationPath" modelBlobId [ begin "0s", dur "1.5s", id (modelBlobId ++ "a1") ]
            , animateBlobPath "updateInternalPath" msgBlobId [ begin (msgBlobId ++ "a1.end"), dur "1.5s" ]
            , animateBlobPath "updateInternalPath" modelBlobId [ begin (modelBlobId ++ "a1.end"), dur "1.5s" ]
            , animateBlobPath "updateInternalPath" newModelId [ begin (modelBlobId ++ "a1.end"), dur "1.5s", id (newModelId ++ "a1") ]
            , animateOpacity newModelId "0" "1" [ begin (modelBlobId ++ "a1.end"), dur "1.5s" ]
            , animateOpacity msgBlobId "0.75" "0" [ begin (modelBlobId ++ "a1.end"), dur "1.5s" ]
            , animateOpacity modelBlobId "1" "0" [ begin (modelBlobId ++ "a1.end"), dur "1.5s" ]
            , animateBlobPath "updateModelPath" newModelId [ begin (newModelId ++ "a1.end"), dur "0.5s", id (newModelId ++ "a2") ]
            , animateBlobPath "modelSetPath" newModelId [ begin (newModelId ++ "a2.end"), dur "0.5s", onEnd (UpdateModelDisplay pendingState.model), id (newModelId ++ "a3") ]
            , animateBlobPath "modelRenderPath" newModelId [ begin (newModelId ++ "a3.end"), dur "1.5s", onEnd (AnimateComplete pendingState) ]
            , animateOpacity newModelId "1" "0" [ begin (newModelId ++ "a3.end+0.75s"), dur "0.5s" ]
            ]


msgBlob color blobId =
    circle
        [ id blobId
        , r "50"
        , cx "0"
        , cy "0"
        , fill color
        , opacity "0.75"
        ]
        []


animateBlobPath pathId blobId attrs =
    animateMotion
        ([ xlinkHref ("#" ++ blobId)
         , fill "freeze"
         ]
            ++ attrs
        )
        [ mpath [ xlinkHref ("#" ++ pathId) ] [] ]


animateOpacity blobId start end attrs =
    animate
        ([ xlinkHref ("#" ++ blobId)
         , attributeName "opacity"
         , from start
         , to end
         , fill "freeze"
         ]
            ++ attrs
        )
        []


lineColor =
    stroke "#333"


lineWidth =
    strokeWidth "3"


ovalPath : ( Int, Int ) -> ( Int, Int ) -> Int -> String
ovalPath ( startX, startY ) ( endX, endY ) offset =
    let
        bezierX =
            toString (startX - offset)

        sX =
            toString startX

        sY =
            toString startY

        eX =
            toString endX

        eY =
            toString endY
    in
        String.join " " [ "M", sX, sY, "C", bezierX, sY, bezierX, eY, eX, eY ]


linePath : ( Int, Int ) -> ( Int, Int ) -> String
linePath ( startX, startY ) ( endX, endY ) =
    String.join " "
        [ "M"
        , toString startX
        , toString startY
        , "L"
        , toString endX
        , toString endY
        ]


modelUpdatePath : String
modelUpdatePath =
    ovalPath ( 200, 650 ) ( 200, 350 ) 80


viewUpdatePath : String
viewUpdatePath =
    ovalPath ( 5, 1000 ) ( 200, 350 ) 80


modelViewPath : String
modelViewPath =
    linePath ( 500, 750 ) ( 500, 850 )


updateModelPath : String
updateModelPath =
    linePath ( 500, 450 ) ( 500, 550 )


updateInternalPath : String
updateInternalPath =
    internalPath ( 200, 350 ) ( 500, 450 )


modelSetPath : String
modelSetPath =
    linePath ( 500, 550 ) ( 500, 650 )


modelRenderPath : String
modelRenderPath =
    linePath ( 500, 650 ) ( 500, 1000 )


modelUpdateAnimationPath : String
modelUpdateAnimationPath =
    (linePath ( 500, 650 ) ( 200, 650 )) ++ modelUpdatePath


internalPath : ( Int, Int ) -> ( Int, Int ) -> String
internalPath ( startX, startY ) ( endX, endY ) =
    String.join
        " "
        [ "M"
        , toString startX
        , toString startY
        , "C"
        , toString (startX + 300)
        , toString startY
        , toString endX
        , toString (endY - 120)
        , toString endX
        , toString endY
        ]


curve : String -> String -> Svg Msg
curve identifier pathDef =
    path
        [ id identifier
        , d pathDef
        , lineColor
        , lineWidth
        , fill "none"
        , markerEnd "url(#arrow)"
        ]
        []


oval : ( Int, Int ) -> Svg Msg
oval ( xInt, yInt ) =
    let
        xStr =
            toString xInt

        yStr =
            toString yInt
    in
        ellipse
            [ fill "white"
            , lineColor
            , lineWidth
            , cx xStr
            , cy yStr
            , rx "300"
            , ry "100"
            ]
            []


viewOval : ( Int, Int ) -> Svg Msg
viewOval ( xInt, yInt ) =
    let
        xStr =
            toString xInt

        yStr =
            toString yInt
    in
        ellipse
            [ fill "white"
            , lineColor
            , lineWidth
            , cx xStr
            , cy yStr
            , rx "500"
            , ry "150"
            ]
            []


modelBlob : ( Int, Int ) -> Int -> String -> SimpleProgram.Model -> Svg Msg
modelBlob ( xPos, yPos ) radius blobId data =
    g [ id blobId ]
        [ circle
            [ r (toString radius)
            , cx (toString xPos)
            , cy (toString yPos)
            , fill "white"
            , lineColor
            , lineWidth
            ]
            []
        , Svg.text_ [ textAnchor "middle", x (toString xPos), y (toString yPos), dy "20px" ]
            [ Svg.text (toString data.count) ]
        ]


invisible : Svg msg -> List (Svg.Attribute msg) -> Svg msg
invisible svg attrs =
    g ((opacity "0") :: attrs) [ svg ]


section : String -> Int -> Int -> String -> Svg Msg
section label top bottom color =
    let
        sectionHeight =
            toString <| bottom - top

        halfway =
            toString <| ((bottom + top) // 2)
    in
        g []
            [ rect [ fill color, x "-800", y (toString top), height sectionHeight, width "3000" ] []
            , Svg.text_ [ textAnchor "left", x "-300", y halfway, dy "20px" ]
                [ Svg.text label ]
            ]


arrowHead : Svg Msg
arrowHead =
    marker
        [ id "arrow"
        , viewBox "0 0 10 10"
        , refX "5"
        , refY "5"
        , markerWidth "10"
        , markerHeight "10"
        , orient "auto"
        ]
        [ path [ d "M 0 0 L 10 5 L 0 10 z" ] [] ]
