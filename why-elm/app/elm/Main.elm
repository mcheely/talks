module Main exposing (main)

import Html exposing (Html, iframe)
import Html.Attributes exposing (class, height, src, width)
import Slides exposing (..)


main : Program Never Model Msg
main =
    Slides.app
        slidesDefaultOptions
        [ mdFragments
            [ "# Unbreakable<sup>*</sup> webapps with Elm"
            , "<sup>*</sup>For a somewhat narrow definition of unbreakable"
            ]
        , mdFragments
            [ """
            Or:

            # How to take your life<sup>*</sup> back with functional programming
             """
            , "<sup>*</sup>At work. Stateless functions probably won't help your personal life."
            ]
        , md """
              ## TL;DR

              * Predictable behavior
              * No runtime exceptions
              * Friendly error messages
              * Reduced application complexity
              * Time traveling debugger
              * Enforced SemVer
              """
        , md
            """
            ## What is Elm?

            * A language in the ML family (Haskell, OCAML)
            * Strong/Static type system
            * Designed for building UI
            * Values are immutable
            * No `null`
            * Made out of functions
            """
        , md
            """
            # Predictable Behavior

            ![](predictable.gif)
            """

        -- JS: The pains of state
        , mdFragments
            [ """
              ## Here's a function.
              ```javascript
              function add (n1, n2) {
                  if (window.oppositeDay) {
                      return n1 - n2;
                  } else {
                      return n1 + n2;
                  }
              }
              ```

              """
            , """
              ## What will this return?

              ```javascript
              add(1,5);
              ```
              """
            ]
        , md " ## How do we deal with this? "
        , md """
             ## Mental models

              ![Moss waves his hands and smiles. "Opposite day is always false."](mental-model.gif)
             """
        , md "## This mostly works..."
        , md """
              ## Until support calls...

              ![There is a fire in the foreground. Moss is typing. He looks concerned.!](support-call.gif)
             """
        , md """
             ## And we realize our mental model was incomplete.

              ![Moss angrily throws his monitor across the room](broken.gif)
             """
        , md """
             ## Now what do we do?

              ```javascript
              function add (n1, n2) {
                  if (window.oppositeDay) {
                      return n1 - n2;
                  } else {
                      return n1 + n2;
                  }
              }
              ```
             """
        , md """
              ## Kick everyone out of your office...

              ![Sherlock points at you. "Get out. I need to go to my mind palace."](mind-palace.gif)
             """
        , md """
              ## And meditate on the code.

              ![Equations swirl as we consider all the possibilities.](hangover-think.gif)
             """

        -- TODO: Until the answer hits you in a moment of genius
        , md """
              ![A cartoon drawing of hundreds of tiny indistinguishable pictures. Somewhere in there is Waldo, but we cannot find him.](waldo2.jpg)
             """
        , md """
              ![](waldo-text2.png)
             """
        , mdFragments
            [ """
              ## How much more code do you need to find out how oppositeDay became true?
              ```javascript
              function add (n1, n2) {
                  if (window.oppositeDay) {
                      return n1 - n2;
                  } else {
                      return n1 + n2;
                  }
              }
              ```

              """
            , "## All of it!"
            ]

        -- JS: Unexpected side effects
        , md "## If our mental models are insufficient, what else can we do?"

        --TODO Edit the gif
        , md """
              ## Tests

              ![Testing is scary.](testing.gif)
             """
        , mdFragments
            [ """
              ## What does this do?

              ```javascript
              multiply(2,3); // Seems pretty obvious to me.
              ```
              """
            , """
              ## Luckily we have tests.

              ```javascript
              assertTrue( 6 == multiply(2,3) ); // It Passes!
              ```
              """
            , """
              ## Thorough tests...

              ```javascript
              window.oppositeDay = true;
              assertTrue( 6 == multiply(2,3) ); // It Still Passes!
              ```
              """
            ]
        , mdFragments
            [ "## But..."
            , """
              ```javascript
              function multiply (n1, n2) {
                  window.oppositeDay = true; // Product made me do it, I swear.
                  remoteApi.igniteServer();  // TODO: Remove this before shipping.
                  return n1 * n2;
              }
              ```

              ![A basket of papers bursts into flames. Beaker panics.](testing-oops.gif)
              """
            ]

        -- Elm: Stateless Fns
        , mdFragments
            [ """
              ## Let's look at some Elm.
              ```elm
              add n1 n2 =
                  if window.oppositeDay then
                      n1 - n2
                  else
                      n1 + n2
              ```
                """
            , """
              ## What will this return?

               ```elm
               add 1 5
               ```
             """
            ]
        , md """
             ## Functions in Elm are stateless

             Return values only depend on arguments and local constants.

             ```elm
             -- This is a constant, and it must be in the same scope as `add`
             window = { oppositeDay = false }

             add n1 n2 =
                 if window.oppositeDay then
                     n1 - n2
                 else
                     n1 + n2
             ```

             This means the answer to "what does this do?" is always nearby.
             """

        -- Elm: Managed Side effects
        , md """
             ## What about our other function?

             ```elm
             multiply n1 n2 =
                 { window | oppositeDay = true }
                 Api.igniteServer
                 n1 * n2
             ```
             """
        , md """
             ## Functions in Elm are isolated

             They cannot change the behavior of other code or trigger arbitrary side-effects.

             ```elm
             multiply n1 n2 =
                 -- Window is immutable, this creates a new object.
                 { window | oppositeDay = true }

                 -- This is a value representing an API call.
                 -- It does nothing on it's own.
                 Api.igniteServer

                 -- This would multiply the two numbers, if this
                 -- function compiled, but it won't.
                 n1 * n2
             ```
             """
        , mdFragments
            [ """
               ## Here's some data

               ```javascript
               {
                  loading: true
               }
               ```

               """
            , """
               ```javascript
               {
                  loading: false,
                  response: { url: "https://i.imgur.com/DmpsoJ6.gif" },
               }
               ```
               """
            , """
               ```javascript
               {
                  loading: false,
                  error: {
                     status: 400,
                     message: "Missing parameter: 'search'"
                  }
               }
               ```
               """
            ]
        , mdFragments
            [ """
               ## More data...

               ```javascript
               {
                 loading: false
               }
               ```
              """
            , """
               ```javascript
               {
                  loading: true,
                  response: { url: "https://i.imgur.com/5zfBSwA.gif" },
               }
               ```
               """
            , """
               ```javascript
               {
                  loading: true,
                  response: { url: "https://i.imgur.com/ZICmpgO.gif" },
                  error: {
                     status: 400,
                     message: "Missing parameter: 'search'"
                  }
               }
               ```
               """
            ]
        , mdFragments
            [ """
              ## What about Elm?

              ```elm
              type RemoteData error result
                  = NotAsked
                  | Loading
                  | Failure error
                  | Success result
              ```
              """
            , "Invalid states can be made impossible."
            ]

        -- No Runtime Exceptions
        , md
            """
              # No Runtime Exceptions

               ![](dont-believe.gif)
              """
        , md """
              ```elm
                main =
                    img [ src (getFirstImage searchJson) ] []


                getFirstImage resultJson =
                    let
                        decodedResponse =
                            Decode.decodeString searchDecoder resultJson

                        firstResult =
                            List.head decodedResponse

                        url =
                            firstResult.url
                    in
                        url
              ```
              """

        -- First error
        , embed "ParserA"
        , md """
              ```
              The argument to function 'head' is causing a mismatch.

              20|             List.head decodedResponse
                                        ^^^^^^^^^^^^^^^
              Function 'head' is expecting the argument to be:

                  List a

              But it is:

                  Result String (List ImgInfo)
              ```

              What's a `Result String (List ImgInfo)`?

              ```elm
              type Result error value
                  = Ok value
                  | Err error
              ```
              """
        , md """
             ```
             'firstResult' does not have a field named 'url'.

              23|             firstResult.url
                              ^^^^^^^^^^^^^^^
              The type of 'firstResult' is:

                  Maybe a

              Which does not contain a field named 'url'.
              ```

              What's a `Maybe a`?

              ```elm
              type Maybe a
                  = Just a
                  | Nothing
              ```
              """
        , md """
              ```elm
                main =
                    img [ src (getFirstImage searchJson) ] []


                getFirstImage resultJson =
                    let
                        decodedResponse =
                            Decode.decodeString searchDecoder resultJson

                        firstResult =
                            List.head decodedResponse

                        url =
                            firstResult.url
                    in
                        url
              ```
              """
        , md """
              ```elm
                main =
                    case getFirstImage searchJson of
                        Ok url ->
                            img [ src url ] []

                        Err message ->
                            text "Unable to load image: " ++ message


                getFirstImage resultJson =
                    let
                        parseResult =
                            Decode.decodeString searchDecoder resultJson
                    in
                        case parseResult of
                            Ok decodedList ->
                                case List.head searchResults of
                                    Just imgDetails ->
                                        Ok imgDetails.url

                                    Nothing ->
                                        Err "No results"

                            Err errMsg ->
                                parseResult
                ```
             """
        , embed "ParserC"
        , md """
            ## A more compact version

            ```elm
            main =
                case getFirstImage searchJson of
                    Ok url ->
                        img [ src url ] []

                    Err message ->
                        text ("Unable to load image: " ++ message)


            getFirstImage resultJson =
                Decode.decodeString searchDecoder resultJson
                    |> Result.andThen
                        (\\searchResults ->
                            List.head searchResults
                                |> (Maybe.map .url)
                                |> (Result.fromMaybe "No results")
                        )
             ```
             """
        , md "# What just happened there?"
        , md
            """
               # Friendly Error Messages

               ![](helpful.gif) vs. ![](unhelpful.gif)
              """
        , embed "Error1"
        , embed "Error2"

        -- Reduced Application Complexity
        , md """
              # Reduced Application Complexity

              (Stuff gets weird)

              ![](complexity.gif)
             """
        , md "![ML has been around as long as C](pl-timeline.jpg)"
        , md
            """
        ### The Elm Architecture (TEA)

        ```elm
        Html.program
            { init = init
            , update = update
            , view = view
            , subscriptions = subs
            }

        init : (Model, Cmd Msg)

        update : Msg -> Model -> (Model, Cmd Msg)

        view : Model -> Html Msg

        subs: Model -> Sub Msg
        ```

        `Model`: Developer defined. Represents *all* application state

        `Msg`: Developer defined. Represents *all* events that change state or cause side-effects

        `Cmd Msg`: Data representing side-effects (e.g. HTTP GET)
        """
        , embed "SimpleExplainer"
        , embed "ComplexExplainer"

        -- Time traveling debugger
        , md
            """
            # Time-travelling debugger

            ![](debugger.gif)
            """
        , frame "debug.html"

        -- Enforced SemVer
        , md
            """
            # Enforced SemVer

            ![](semver.gif)
            """
        , mdFragments
            [ "## The package manager analyzes exposed APIs and enforces:"
            , " * First published version is always 1.0.0"
            , " * If you add to your API, a minor bump is required."
            , " * If you break API compatibility, a major bump is required."
            , " * If your API doesn't change, a patch bump is fine."
            ]
        , md "![The End.](the_end.gif)"
        ]


embed : String -> Slide
embed moduleName =
    frame ("loader.html?" ++ moduleName)


frame : String -> Slide
frame page =
    html <|
        iframe
            [ class "page-embed"
            , src page
            , height 600
            , width 1100
            ]
            []
