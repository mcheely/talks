module GifProgram exposing (..)

import Html exposing (Html, text, button, div, img)
import Html.Attributes exposing (src, class)
import Html.Events exposing (onClick)
import Http
import Json.Decode as Decode exposing (Decoder)


main =
    Html.program
        { init = init
        , update = update
        , view = view
        , subscriptions = subs
        }



-- Model


type alias Model =
    { imgUrl : String }


init : ( Model, Cmd Msg )
init =
    ( { imgUrl = notFoundImg }, Cmd.none )


giphyKey : String
giphyKey =
    "vSKgXSeeaqqZZMy4huWtGhiJfygTmtpC"


notFoundImg : String
notFoundImg =
    "https://media.giphy.com/media/26xBIygOcC3bAWg3S/giphy.gif"


searchDecoder : Decoder String
searchDecoder =
    Decode.at [ "data", "image_url" ] Decode.string



-- Update


type Msg
    = Search
    | SearchResults (Result Http.Error String)


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        Search ->
            ( model, catSearch )

        SearchResults result ->
            let
                url =
                    Result.withDefault notFoundImg result
            in
                ( { model | imgUrl = url }, Cmd.none )


catSearch : Cmd Msg
catSearch =
    Http.get ("https://api.giphy.com/v1/gifs/random?tag=cat&rating=g&api_key=" ++ giphyKey)
        searchDecoder
        |> Http.send SearchResults



-- View


view : Model -> Html Msg
view model =
    div [ class "cat-picker" ]
        [ div [ class "cat-image" ]
            [ img [ src model.imgUrl ] [] ]
        , button [ onClick Search ] [ text "CATS!" ]
        ]



-- Subscriptions


subs : Model -> Sub Msg
subs model =
    Sub.none
