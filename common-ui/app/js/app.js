import { Elm } from "../elm/Main.elm";
import * as components from "../../node_modules/@genesys/common-webcomponents/dist/genesys-webcomponents.js";

hljs.initHighlightingOnLoad();

function doHighlight() {
  document.querySelectorAll("pre code").forEach(block => {
    hljs.highlightBlock(block);
  });
}

document.addEventListener("transitionrun", doHighlight);
document.addEventListener("transitionend", doHighlight);

let embedNode = document.getElementById("app");

Elm.Main.init({
  node: embedNode
});

let fsCtl = document.createElement("div");
document.body.appendChild(fsCtl);
fsCtl.setAttribute("class", "fs-control");
fsCtl.innerHTML = '<img src="fullscreen-enter.svg">';
fsCtl.onclick = () => {
  document.querySelector("div").requestFullscreen();
};
