module Slides exposing (centered, dynamic, md, mdTitle, titled, ui, withTitle)

import Element exposing (Element, column, fill, height, width)
import Element.Font as Font
import Html exposing (Html, div, text)
import Html.Attributes exposing (class, style)
import Markdown
import Presentation exposing (Slide, asSlide, dynamicSlide)


withTitle : String -> Html msg -> Html msg
withTitle title content =
    div [ class "title-wrapper" ]
        [ div [ class "slide-title" ] [ text title ]
        , div [ class "slide-content" ] [ content ]
        , div [ class "slide-footer" ] []
        ]


centered : Html msg -> Slide model msg
centered html =
    div
        [ style "text-align" "center"
        , style "display" "flex"
        , style "height" "100%"
        , style "align-items" "center"
        , style "justify-content" "center"
        ]
        [ html ]
        |> asSlide


md : String -> Slide model msg
md text =
    Markdown.toHtml [ style "text-align" "left" ] text
        |> centered


mdTitle : String -> String -> Slide model msg
mdTitle title markdown =
    Markdown.toHtml [ style "text-align" "left" ] markdown
        |> withTitle title
        |> centered


titled : String -> Html msg -> Slide model msg
titled title content =
    withTitle title content
        |> centered


ui : String -> Element msg -> Slide moddel msg
ui title element =
    titled title (elmUi element)


elmUi : Element msg -> Html msg
elmUi element =
    div [ class "elm-ui" ]
        [ Element.layout
            [ Element.width Element.fill
            , Element.height Element.fill
            , Font.family [ Font.typeface "Roboto", Font.sansSerif ]
            , Font.color (Element.rgb255 68 74 82)
            ]
            element
        ]


dynamic : (model -> Html msg) -> Slide model msg
dynamic viewFn =
    dynamicSlide viewFn
