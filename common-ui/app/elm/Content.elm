module Content exposing (slides)

import Component
import Element exposing (Element, centerX, column, el, fill, height, image, padding, row, spaceEvenly, width)
import Html exposing (..)
import Html.Attributes exposing (attribute, class, href, src, title)
import Markdown
import Presentation exposing (asSlide)
import Sim
import Slides exposing (centered, dynamic, md, mdTitle, titled, ui, withTitle)


uitext =
    Element.text


slides simMsg comMsg =
    [ asSlide titlePage
    , mdTitle "Why Common UI?"
        """
* Efficiency
  * Don't want to maintain multiple design systems
  * Don't need to implement data tables for each combination of product/technology
  * Want to share feature implementations where possible
* Evolution
  * Technology will change in the coming years - how will we keep up
* Collaboration
  * Sharing best-practices across teams has value
  * Having common code to work on can build a more technically cohesive organization
        """

    -- HISTORY
    , md "# Genesys UI History Lessons"
    , ui "Frameworks over the last 5 years" history
    , md "# What Can We Learn From This?"
    , mdTitle "The Cynical Take"
        """
* UI developers chase fads
* Our applications are too complex
* We need to reduce technology churn
* UI tools should be mandated by architects/management
      """
    , mdTitle "Problems With Fixed Technology"
        """
* Any technology shift requires a re-write of shared tools
* If the mandated tech stops being maintained, you're in trouble
* Risk of developer stagnation
* Inability to use the best tool for the job
* Who gets a voice in the process?
* Who make the final decision?
         """
    , mdTitle "A Better Analysis"
        """
* Web app development best practices are still evolving
   * Virtual DOM
   * Two-way data binding
* We provide a diverse set of features for our customers
   * Agent Interaction Handling, Real-time Analytics, Document Management
      * Different SLA's
      * Different implementation challenges
      * Different people
* Acquisitions and changing business needs mean that new technology is inevitable
* UI architectures must embrace evolution and heterogeneous systems
       """
    , mdTitle "PureCloud Delivery Simulation"
        """
* PureCloud is designed as a single user-facing experience
    * This is about what happens when that's shipped as a single artifact
* Weekly shipping cadence
* High SLA
* Growing number of independent features/teams
* These lessons will apply in other situations where independent features are embedded together
    """
    , dynamic (Sim.view >> Html.map simMsg)
    , mdTitle "Lessons Learned"
        """
* Highly coupled systems are a problem
    * Can drag down deployment cadence
    * Can also result in unexpected bugs
        * Dependency changes
        * Uncaught Exceptions
* Independent deployments and runtime isolation can solve these problems
    * Problems only impact a single feature's deploy cadence
    * Runtime failure impact is limited
        * Not possible to crash the whole app
        * Can only break specific integrations
    """

    -- MICROSERVICES
    , md "# Why are we talking about microservices?"
    , mdTitle "Benefits of microservices"
        """
From AWS whitepaper: [Microservices on AWS](https://docs.aws.amazon.com/aws-technical-content/latest/microservices-on-aws/)

* Agility
* Innovation
* Quality
* Scalability
* Availability
        """
    , mdTitle "Benefits of Microservices"
        """
**Agility**

Microservices foster an organization of small _independent_ teams that take ownership of their services. Teams act within a small and well-understood _bounded context_, and they are empowered to _work independently_ and quickly, thus _shortening cycle times_...

https://docs.aws.amazon.com/aws-technical-content/latest/microservices-on-aws/benefits-of-microservices.html#agility
    """
    , mdTitle "Benefits of Microservices"
        """

**Innovation**

The fact that small teams can _act autonomously_ and _choose the appropriate technologies, frameworks, and tools_ for their domains is an important driver for _innovation_. Responsibility and accountability foster a culture of ownership...

https://docs.aws.amazon.com/aws-technical-content/latest/microservices-on-aws/benefits-of-microservices.html#innovation
    """
    , mdTitle "Benefits of Microservices"
        """

**Quality**

Organizing software engineering around microservices can also improve the quality of code. The benefits of dividing software into _small and well-defined modules_ are ... improved _reusability_, _composability_, and _maintainability_ of code.

https://docs.aws.amazon.com/aws-technical-content/latest/microservices-on-aws/benefits-of-microservices.html#quality
    """
    , mdTitle "Benefits of Microservices"
        """

**Scalability**

Fine-grained _decoupling_ of microservices is a best practice for building large-scale systems... it allows choosing the _appropriate and optimal technologies_... Each service can be implemented with the appropriate programming languages and frameworks...

https://docs.aws.amazon.com/aws-technical-content/latest/microservices-on-aws/benefits-of-microservices.html#scalability
    """
    , mdTitle "Benefits of Microservices"
        """

**Availability**

Microservices architectures make it easier to implement _failure isolation_...

https://docs.aws.amazon.com/aws-technical-content/latest/microservices-on-aws/benefits-of-microservices.html#availability
       """
    , mdTitle "Challenges of Microservices"
        """
AWS whitepaper divides challenges into two categories:
* Architectural Complexity
* Operational Complexity

UIs have some features that mitigate some of these challenges:
* Web Apps all run on the same "VM" (the browser)
* UI structure is hierarchical
* UI subsystem interactions are limited
* UI component dependencies are typically soft
* UI communication layer can be centrally managed
* UI has only one consumer per machine
* Few UI subsystems run concurrently
        """
    , mdTitle "Our Approach to Common UI"
        """
* Web-Components for shared UI widgets with a common design
    * Reusable, Composable
* Iframes for large-scale feature integration
    * Reusable, Composable, Isolated
* Standard set of deployment tools for the common environment (PureCloud)
    """
    , mdTitle "Web Components (What are they?)"
        """
* Web components are a combination of several browser APIs
* They allow reusable interfaces to be defined as browser elements

```
<div class="im-a-native-element">
    <gux-accordian class="im-a-web-component">
        <div slot="First Section">
            Here's a button: <gux-button text="Click me!"></gux-button>
        </div>
        <div slot="Second Section">
            Here's a toggle: <gux-toggle></gux-toggle>.
        </div>
    </gux-accordian>
</div>
```

       """
    , mdTitle "Web Components"
        """
* Just another DOM element
  * Can be used _anywhere_ with _any_ framework
  * Almost like having a library that can be consumed by any language
  * Can be shared without re-working features
  * Internal implementations can evolve while maintaining the external API
       """
    , mdTitle "The Worst Place to Embed"
        """
* A less commonly used framework
* Not written in JavaScript (TIOBE index 51-100)
* No direct calls to JavaScript functions
        """
    , dynamic
        (\model ->
            Component.view model
                |> Html.map comMsg
                |> withTitle "This Presentation is Written in Elm!"
        )
    , mdTitle "Iframe Coordinator"
        """
* Enables microservice patterns
  * Full runtime isolation
  * Independent deployments
* Library can provide common APIs for things like
  * Notifications
  * Logging
  * Dialogs
* General host-client pub-sub
        """
    , mdTitle "Iframe Coordinator (Host)"
        """
```
<body>
    <!-- host-app stuff -->
    <frame-router id="frame-element" route="/app1/somePage">
    <!-- more host-app stuff -->
</body>
```

```
document.getElementById("frame-element").setupFrames({
  client1: {
    assignedRoute: "/app1",
    url: "https://apps.mypurecloud.com/component/example1/"
  },
  client2: {
    assignedRoute: "/app2",
    url: "https://apps.mypurecloud.com/component/example2/"
  }
}, {
  locale: 'en-US',
  hostRootUrl: "https://apps.mypurecloud.com/directory/"
});
```
        """
    , mdTitle "Iframe Coordinator (Client)"
        """
```
import { Client } from "iframe-coordinator/client.js";

const client = new Client();
client.start();

client.requestToast({
  title: "Hello World!",
  message: "I'm a client toast."
});
```
```
<body>
    <!-- client stuff -->
    <a href="${hostAppUrl}/${hostAppRoute}">Click Me</a>
    <!-- more client stuff -->
</body>
```
        """
    , mdTitle "Deployment Tools"
        """
* CLI tools
    * Easy to integrate into any CI/CD process
* Installed via npm
    * Versioned with Semver
    * Allows projects to install tools a dev dependency
* Can pull version/build data from a manifest file
    * Useful for carrying data across CI jobs
* Two stage upload-deploy process
    * Enables running old & future versions in all environments
    * Very useful for support case triage
    * Enables early access to in-progress features
        """
    , mdTitle "Deployment Tools"
        """
```
export CDN=$(cdn --ecosystem gmsc --manifest manifest.json)
# ... app build ...
upload --ecosystem gmsc --manifest manifest.json --source-dir ./dist
deploy --ecosystem gmsc --manifest manifest.json --dest-env dev

```
        """
    , mdTitle "Using The Common UI Tooling"
        """
* Everything is installable via npm:
  * `@genesys/common-webcomponents`
  * `iframe-coordinator`
  * `@purecloud/web-app-deploy`
* Documentation is available at:
    https://apps.gmscdev.com/common-ui-docs
  * Docs are built using iframe-coordinator and will eventually serve as
example apps as well as documentation.
        """
    , md "# Development Process"
    , mdTitle "Open Source Model"
        """
* Core team
    * Crosses PureConnect, PureEngage, and PureCloud
    * Organize the project
    * Review submissions
    * Contribute to high priority items
* External contributors
    * Contribute to meet their needs
    * Eventually become core members
* Contribution isn't just code
    * Docs
    * Issues
    * Feedback
        """
    , titled "Core Team"
        (teamList
            [ ( "Jarrod Stormo", "stormo.jpg" )
            , ( "Chris Covert", "chris.jpg" )
            , ( "Jérémie Pichon", "jeremie.jpg" )
            , ( "Keri Lawrence", "keri.jpg" )
            , ( "Matt Cheely", "matt.jpg" )
            ]
        )
    , titled "Other Contributors"
        (div [ class "team-columns" ]
            [ teamList
                [ ( "Andy Kauffman", "andy.jpg" )
                , ( "Boris Nicolas", "boris.jpg" )
                , ( "UX Team", "ux.jpg" )
                , ( "Colin Leonard", "colin.jpg" )
                ]
            , teamList
                [ ( "Justin Ray", "justin.jpg" )
                , ( "Eric Lifka", "eric.jpg" )
                , ( "Darragh Kirwan", "darragh.jpg" )
                , ( "William Clifford", "william.jpg" )
                ]
            , teamList
                [ ( "Eoin Joyce", "eoin.jpg" )
                , ( "Will Hartzell-Baird", "will.jpg" )
                , ( "You", "you.png" )
                ]
            ]
        )
    , mdTitle "Contributing"
        """
* Get access to [PureCloud JIRA](https://inindca.atlassian.net) (sign up with your genesys email)
* Request access to PureCloud Bitbucket
* Read the contributing guidelines for the project
    * [Web Components](https://bitbucket.org/inindca/genesys-webcomponents/src/master/CONTRIBUTING.md)
* Open lines of communication with the team
    * [Pure Cloud Group](https://apps.mypurecloud.com/directory/#/group/5b99076f08ece9148419013b)
    * CommonUIDevelopment@genesys.com
        """
    , md "# Questions?"
    , md ""
    , mdTitle "Challenges of Microservices"
        """
**Architectural Complexity**

* In monolithic architectures, the complexity and the number of dependencies reside inside the code base, while in microservices architectures complexity moves to the interactions of the individual services
  * UI structure is hierarchical
  * UI communication layer can be centrally managed
  * UI subsystem interactions are limited
  * Few UI subsystems run concurrently
        """
    , mdTitle "Challenges of Microservices"
        """
**Operational Complexity**

* How to provision resources in a scalable and cost-efficient way?
  * UI has only one consumer per machine
* How to operate dozens or hundreds of microservice components effectively without multiplying efforts?
  * This is part of the common UI goal, we'll talk about that.
  * UI structure is hierarchical
* How to avoid reinventing the wheel across different teams and duplicating tools and processes?
  * This is part of the common UI goal, we'll talk about that.
  * UI structure is hierarchical
* How to keep track of hundreds of pipelines of code deployments and their interdependencies?
  * Dependencies are generally soft between UI components
        """
    , mdTitle "Challenges of Microservices"
        """
**Operational Complexity**

* How to track and debug interactions across the whole system?
  * UI subsystem interactions are typically limited
  * Dependencies are generally soft between UI components
  * Few UI subsystems run concurrently
* How to analyze high amounts of log data in a distributed application that quickly grows and scales beyond anticipated demand?
  * UI structure is hierarchical
  * We can re-use existing service/platform tools
        """
    , mdTitle "Challenges of Microservices"
        """
**Operational Complexity**

* How to deal with a lack of standards and heterogeneous environments that include different technologies and people with differing skill sets?
  * This is a management and technical leadership problem (out of scope)
* How to value diversity without locking into a multiplicity of different technologies that need to be maintained and upgraded over time?
  * This is a management and technical leadership problem (out of scope)
* How to monitor overall system health and identify potential hotspots early on?
  * UI structure is hierarchical
  * UI subsystem interactions are limited
  * Few UI subsystems run concurrently
* How to deal with versioning?
  * UI communication layer can be centrally managed
  * UI subsystem interactions are limited
        """
    , mdTitle "Challenges of Microservices"
        """
**Operational Complexity**

* How to ensure that services are still in use especially if the usage pattern isn’t consistent?
  * UI usage tracking is fairly well understood (out of scope)
  * Can re-use existing service/platform tools
* How to ensure the proper level of decoupling and communication between services?
  * UI subsystem interactions are typically limited
  * UI communication layer can largely be centrally managed
        """
    ]



{-
   Talk about why there is a need for common UI

       Talk about the history and evolution of our platforms.
       Being flexible to new acquisitions like altocloud
       Costs and tradeoffs
       Flexibility in our ability to deliver. Ie Not forcing teams to all use the same tools and technologies.

   Talk about our tools.

       Iframe coordinator and web components.
       Team recognition

   How to contribute

       Talk about delivery model.
       Go over UX guidelines.
       Who to reach out to.
-}


titlePage : Html msg
titlePage =
    div [ class "title-page" ]
        [ div [ class "main-section" ]
            [ img
                [ class "g-logo"
                , src "logo-white.svg"
                ]
                []
            , div [ class "divider" ]
                []
            , div [ class "title-text" ]
                [ text "Common UI: Bringing the advantages of microservices to front-end development" ]
            ]
        , div [ class "small-section" ]
            [ div [ class "left-side" ]
                [ img [ class "genesys-small", src "logo-small.gif" ] []
                , div [ class "small-divider" ] []
                , div [ class "pko-text" ] [ text "PKO19" ]
                ]
            , div [ class "talk-link" ]
                [ a [ href "https://mattcheely.github.io/talks/common-ui/" ]
                    [ text "https://mattcheely.github.io/talks/common-ui/" ]
                ]
            ]
        ]


teamList : List ( String, String ) -> Html msg
teamList people =
    div [ class "team-list" ]
        (List.map
            (\( name, photo ) ->
                div [ class "team-member" ]
                    [ img [ src photo ] []
                    , span [ class "member-name" ] [ text name ]
                    ]
            )
            people
        )


history : Element msg
history =
    column [ width fill, height fill, spaceEvenly ]
        [ pureEngageHistory
        , pureConnectHistory
        , pureCloudHistory
        , yearLabels [ 2013, 2014, 2015, 2016, 2017, 2018 ]
        ]


techIcon path =
    div [ class "tech-icon" ]
        [ img [ src path ] []
        ]


angularjs =
    techIcon "angularjs.png"


angular =
    techIcon "angular.svg"


knockout =
    techIcon "knockout.png"


ember =
    techIcon "ember.png"


elm =
    techIcon "elm.svg"


ext =
    techIcon "sencha.png"


vue =
    techIcon "vue.svg"


backbone =
    techIcon "backbone.png"


react =
    techIcon "react.png"


pureEngageHistory =
    historyRow
        [ productIcon "PureEngage.png"
        , techList [ ext, backbone, angularjs ]
        , techList [ ext, backbone, angularjs ]
        , techList [ angular, backbone ]
        , techList [ angular, backbone ]
        , techList [ angular, backbone, react ]
        , techList [ angular, backbone, react, vue ]
        ]


pureConnectHistory =
    historyRow
        [ productIcon "PureConnect.png"
        , techList [ angularjs ]
        , techList [ angularjs ]
        , techList [ angularjs ]
        , techList [ angularjs ]
        , techList [ angular, angularjs ]
        , techList [ angular, angularjs ]
        ]


pureCloudHistory =
    historyRow
        [ productIcon "PureCloud.png"
        , techList [ knockout ]
        , techList [ knockout, ember ]
        , techList [ ember, knockout, angularjs ]
        , techList [ ember, knockout ]
        , techList [ ember, knockout, elm ]
        , techList [ ember, knockout, angular, elm ]
        ]


productIcon url =
    image [ width fill ]
        { src = url
        , description = "Pure Cloud"
        }


techList libraries =
    el [ width fill ]
        (ul [ class "tech-list" ]
            (List.map
                (\name ->
                    li [] [ name ]
                )
                libraries
            )
            |> Element.html
        )


yearLabels years =
    historyRow ([ el [] (uitext " ") ] ++ List.map (String.fromInt >> uitext) years)


historyRow columns =
    row [ spaceEvenly, width fill ] (List.map historyColumn columns)


historyColumn element =
    el [ width fill ]
        (el [ centerX ] element)


componentDemo : Html msg
componentDemo =
    accordion []
        [ div [ slot "First Section" ]
            [ span [] [ text "Here is a button:  " ]
            , button [ textAttr "Click Me!" ] []
            ]
        , div [ slot "Second Section" ]
            [ span [] [ text "Here's a toggle: " ]
            , toggle [] []
            ]
        ]


toggle =
    Html.node "gux-toggle"


accordion =
    Html.node "gux-accordion"


button =
    Html.node "gux-button"


textAttr =
    attribute "text"


slot =
    attribute "slot"
