module Component exposing (Msg, update, view)

import Html exposing (Html, code, div, p, pre, span, text)
import Html.Attributes exposing (attribute, class)
import Html.Events exposing (onCheck, onClick)
import Sim exposing (Model)


-- Update


type Msg
    = ButtonClicked
    | ToggleFlipped Bool


update : Msg -> Model -> Model
update msg model =
    case msg of
        ButtonClicked ->
            { model | buttonClicked = model.buttonClicked + 1 }

        ToggleFlipped on ->
            { model | toggleState = on }



-- View


view : Model -> Html Msg
view model =
    div []
        [ div []
            [ p []
                [ text "Embedding our web components was trivial!" ]
            , div [ class "bound-data" ]
                [ div []
                    [ text "Button Clicked "
                    , text (String.fromInt model.buttonClicked)
                    , text " times."
                    ]
                , div []
                    [ text "Toggle is "
                    , if model.toggleState then
                        text "on."
                      else
                        text "off."
                    ]
                ]
            ]
        , div [ class "component-demo" ]
            [ pre [ class "code" ]
                [ code [ class "elm" ]
                    [ text """view =
  accordion []
      [ div [ slot "First Section" ]
          [ span [] [ text "Here's a button:  " ]
          , button [ textAttr "Click Me!"
                   , onClick ButtonClicked
                   ]
              []
          ]
      , div [ slot "Second Section" ]
          [ span [] [ text "Here's a toggle: " ]
          , toggle [ onCheck ToggleFlipped ] []
          ]
      ]
"""
                    ]
                ]
            , div [ class "component-rendering" ]
                [ componentDemo
                ]
            ]
        ]


componentDemo : Html Msg
componentDemo =
    accordion []
        [ div [ slot "First Section" ]
            [ span [] [ text "Here's a button:  " ]
            , button [ textAttr "Click Me!", onClick ButtonClicked ]
                []
            ]
        , div [ slot "Second Section" ]
            [ span [] [ text "Here's a toggle: " ]
            , toggle [ onCheck ToggleFlipped ] []
            ]
        ]


toggle =
    Html.node "gux-toggle"


accordion =
    Html.node "gux-accordion"


button =
    Html.node "gux-button"


textAttr =
    attribute "text"


slot =
    attribute "slot"
