module Sim exposing (Model, Msg, init, update, view)

import Html exposing (Html, button, dd, div, dl, dt, h1, h2, input, label, span, text)
import Html.Attributes as Attr exposing (class, classList, for, id, step, type_, value)
import Html.Events exposing (onClick, onInput)
import Random exposing (Generator)


type alias Model =
    { projectCount : Int
    , majorFeaturePercent : Int
    , seriousIssuePercent : Int
    , simResult : Maybe YearSim
    , buttonClicked : Int
    , toggleState : Bool
    }


type alias YearSim =
    { weeks : List WeekSim
    }


type alias WeekSim =
    { projects : List ProjectReleaseSim
    }


type alias ProjectReleaseSim =
    { majorFeature : Bool
    , seriousIssue : Bool
    }


type alias Project =
    { id : Int
    }


init : ( Model, Cmd Msg )
init =
    let
        initialModel =
            { projectCount = 1
            , majorFeaturePercent = 25
            , seriousIssuePercent = 5
            , simResult = Nothing
            , buttonClicked = 0
            , toggleState = False
            }
    in
    ( initialModel, simulateYear initialModel )


weekFeatureCount : WeekSim -> Int
weekFeatureCount weekSim =
    List.filter .majorFeature weekSim.projects
        |> List.length


weekBadFeatureCount : WeekSim -> Int
weekBadFeatureCount weekSim =
    List.filter (\p -> p.majorFeature && p.seriousIssue) weekSim.projects
        |> List.length


hasIssue : WeekSim -> Bool
hasIssue weekSim =
    List.any .seriousIssue weekSim.projects



-- Update


type Msg
    = UpdateProjectCount String
    | UpdateFeaturePercent String
    | UpdateIssuePercent String
    | SimComplete YearSim
    | DoSim


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        UpdateProjectCount string ->
            String.toInt string
                |> Maybe.map (updateProjectCount model >> andUpdateSim)
                |> Maybe.withDefault ( model, Cmd.none )

        UpdateFeaturePercent string ->
            String.toInt string
                |> Maybe.map (updateFeaturePercent model >> andUpdateSim)
                |> Maybe.withDefault ( model, Cmd.none )

        UpdateIssuePercent string ->
            String.toInt string
                |> Maybe.map (updateIssuePercent model >> andUpdateSim)
                |> Maybe.withDefault ( model, Cmd.none )

        SimComplete yearSim ->
            ( { model | simResult = Just yearSim }, Cmd.none )

        DoSim ->
            model |> andUpdateSim


updateProjectCount : Model -> Int -> Model
updateProjectCount model count =
    if count < 1 then
        { model | projectCount = 1 }
    else
        { model | projectCount = count }


updateFeaturePercent : Model -> Int -> Model
updateFeaturePercent model percent =
    if percent < 1 then
        { model | majorFeaturePercent = 1 }
    else
        { model | majorFeaturePercent = percent }


updateIssuePercent : Model -> Int -> Model
updateIssuePercent model percent =
    if percent < 1 then
        { model | seriousIssuePercent = 1 }
    else
        { model | seriousIssuePercent = percent }


andUpdateSim : Model -> ( Model, Cmd Msg )
andUpdateSim model =
    ( model, simulateYear model )


simulateYear : Model -> Cmd Msg
simulateYear model =
    Random.generate SimComplete (yearSimGenerator model)


yearSimGenerator : Model -> Generator YearSim
yearSimGenerator model =
    Random.map YearSim
        (Random.list 52 (releaseWeekGenerator model))


releaseWeekGenerator : Model -> Generator WeekSim
releaseWeekGenerator model =
    Random.map WeekSim
        (Random.list model.projectCount (releaseGenerator model))


releaseGenerator : Model -> Generator ProjectReleaseSim
releaseGenerator model =
    Random.map2 ProjectReleaseSim
        (Random.int 1 100
            |> Random.map (\int -> int <= model.majorFeaturePercent)
        )
        (Random.int 1 100
            |> Random.map (\int -> int <= model.seriousIssuePercent)
        )



-- View


view : Model -> Html Msg
view model =
    let
        expectedImpact =
            calculateBugImpact model
    in
    div [ class "sim" ]
        [ div [ class "sim-options" ]
            [ settingsView model
            , Maybe.map (simSummary expectedImpact) model.simResult
                |> Maybe.withDefault (text "")
            ]
        , Maybe.map weeklyBreakdown model.simResult
            |> Maybe.withDefault (text "")
        ]


simSummary : ExpectedImpact -> YearSim -> Html Msg
simSummary expected sim =
    let
        featureCount =
            List.map weekFeatureCount sim.weeks
                |> List.sum

        successfulFeatureCount =
            List.filter (not << hasIssue) sim.weeks
                |> List.map weekFeatureCount
                |> List.sum

        badFeatureCount =
            List.map weekBadFeatureCount sim.weeks
                |> List.sum

        impactedFeatureCount =
            featureCount - successfulFeatureCount - badFeatureCount
    in
    div [ class "release-legend" ]
        [ h1 []
            [ text "Simulated Year" ]
        , legendItem
            "all-types"
            "Completed Features"
            featureCount
            (round expected.yearlyFeatures)
        , legendItem
            ""
            "Successful Feature Releases"
            successfulFeatureCount
            (round expected.goodFeatures)
        , legendItem
            "has-issue"
            "Release-Impacted Features"
            badFeatureCount
            (round expected.badFeatures)
        , legendItem
            "was-impacted"
            "Features Impacted by Other Projects"
            impactedFeatureCount
            (round expected.impactedFeatures)
        ]


legendItem : String -> String -> Int -> Int -> Html Msg
legendItem extraClass label count expected =
    div [ class "legend-item" ]
        [ span [ class ("release " ++ extraClass) ] []
        , span []
            [ text
                (" "
                    ++ String.fromInt count
                    ++ " "
                    ++ label
                    ++ " ("
                    ++ String.fromInt expected
                    ++ " expected)"
                )
            ]
        ]


weeklyBreakdown : YearSim -> Html Msg
weeklyBreakdown sim =
    div []
        [ h1 [] [ text "Weekly Release Events" ]
        , div [ class "year-sim" ] (List.indexedMap weekVis sim.weeks)
        ]


weekVis : Int -> WeekSim -> Html Msg
weekVis idx weekSim =
    let
        week =
            idx + 1
    in
    div
        [ classList
            [ ( "week-sim", True )
            , ( "has-issue", hasIssue weekSim )
            ]
        ]
        (div [ class "week-label" ] [ text (String.fromInt week) ]
            :: List.map projectVis weekSim.projects
        )


projectVis : ProjectReleaseSim -> Html Msg
projectVis project =
    div
        [ classList
            [ ( "release", True )
            , ( "has-issue", project.seriousIssue )
            , ( "has-feature", project.majorFeature )
            ]
        ]
        []


settingsView : Model -> Html Msg
settingsView model =
    div [ class "settings-view" ]
        [ h1 [] [ text "Product Release Parameters" ]
        , projectCountSetting model.projectCount
        , featureShipRateSetting model.majorFeaturePercent
        , issuePercentSetting model.seriousIssuePercent
        , div [ class "settings-actions" ]
            [ button [ class "refresh-sim", onClick DoSim ] [ text "Re-Run" ]
            ]
        ]


issuePercentSetting : Int -> Html Msg
issuePercentSetting issuePercent =
    intSetting
        { label = "Release Impacting Issue Probability"
        , id = "issueProb"
        , min = 1
        , max = 100
        , currentValue = issuePercent
        , onChange = UpdateIssuePercent
        , formatter = \val -> String.fromInt val ++ "%"
        }


featureShipRateSetting : Int -> Html Msg
featureShipRateSetting featurePercent =
    intSetting
        { label = "Major Feature Release Rate"
        , id = "featureProb"
        , min = 1
        , max = 100
        , currentValue = featurePercent
        , onChange = UpdateFeaturePercent
        , formatter = \val -> String.fromInt val ++ "%"
        }


projectCountSetting : Int -> Html Msg
projectCountSetting projectCount =
    intSetting
        { label = "Feature Teams"
        , id = "projectCount"
        , min = 1
        , max = 25
        , currentValue = projectCount
        , onChange = UpdateProjectCount
        , formatter = String.fromInt
        }


intSetting :
    { label : String
    , id : String
    , min : Int
    , max : Int
    , currentValue : Int
    , onChange : String -> Msg
    , formatter : Int -> String
    }
    -> Html Msg
intSetting opts =
    div [ class "range-setting" ]
        [ label [ for opts.id ] [ text opts.label ]
        , input
            [ id opts.id
            , type_ "range"
            , Attr.min (String.fromInt opts.min)
            , Attr.max (String.fromInt opts.max)
            , step "1"
            , value (String.fromInt opts.currentValue)
            , onInput opts.onChange
            ]
            []
        , span
            [ class "val-display" ]
            [ text (opts.formatter opts.currentValue) ]
        ]


countView : String -> Int -> Html Msg
countView label count =
    div []
        [ text label, text ": ", text (String.fromInt count) ]


percentView : String -> Int -> Html Msg
percentView label percent =
    div []
        [ text label, text ": ", text (String.fromInt percent), text "%" ]


type alias ExpectedImpact =
    { yearlyFeatures : Float
    , goodFeatures : Float
    , badFeatures : Float
    , impactedFeatures : Float
    }


calculateBugImpact : Model -> ExpectedImpact
calculateBugImpact model =
    let
        seriousIssueRate =
            toFloat model.seriousIssuePercent / 100

        featureRate =
            toFloat model.majorFeaturePercent / 100

        projectCount =
            toFloat model.projectCount

        noIssueProbability =
            (1 - seriousIssueRate) ^ projectCount

        issueProbability =
            1 - noIssueProbability

        expectedFeaturesPerWeek =
            featureRate * projectCount

        expectedYearlyFeatures =
            expectedFeaturesPerWeek * 52

        expectedBadFeatures =
            seriousIssueRate * featureRate * projectCount * 52

        expectedGoodFeatures =
            noIssueProbability * expectedFeaturesPerWeek * 52

        expectedImpactedFeatures =
            expectedYearlyFeatures - expectedGoodFeatures - expectedBadFeatures
    in
    { yearlyFeatures = expectedYearlyFeatures
    , goodFeatures = expectedGoodFeatures
    , badFeatures = expectedBadFeatures
    , impactedFeatures = expectedImpactedFeatures
    }
