module Presentation exposing (Msg(..), Presentation, Slide, asSlide, create, dynamicSlide, subscriptions, update, view)

import Browser.Events exposing (onKeyDown)
import Html exposing (Html, div, text)
import Html.Attributes exposing (style)
import Html.Events exposing (on)
import Json.Decode as Decode


type alias Program model msg =
    { init : ( model, Cmd msg )
    , update : msg -> model -> ( model, Cmd msg )
    , view : model -> Html msg
    , subscriptions : model -> Sub msg
    }


type Slide model msg
    = Static (Html msg)
    | Live (model -> Html msg)


toHtml : Slide model msg -> model -> Html msg
toHtml slide model =
    case slide of
        Static html ->
            html

        Live viewFn ->
            viewFn model


asSlide : Html msg -> Slide model msg
asSlide html =
    Static html


dynamicSlide : (model -> Html msg) -> Slide model msg
dynamicSlide viewFn =
    Live viewFn


type Presentation model msg
    = Displaying (List (Slide model msg)) (Slide model msg) (List (Slide model msg))
    | TransitionForward (List (Slide model msg)) (Slide model msg) (Slide model msg) (List (Slide model msg))
    | TransitionBack (List (Slide model msg)) (Slide model msg) (Slide model msg) (List (Slide model msg))


create : List (Slide model msg) -> Presentation model msg
create slides =
    case slides of
        first :: rest ->
            Displaying [] first rest

        [] ->
            Displaying [] (Static (div [] [])) []



-- Update


type Msg
    = Previous
    | Next
    | AnimationFinished
    | NoOp


update : Msg -> Presentation model msg -> Presentation model msg
update msg presentation =
    case msg of
        Next ->
            advance presentation

        Previous ->
            regress presentation

        AnimationFinished ->
            completeTransition presentation

        NoOp ->
            presentation


advance : Presentation model msg -> Presentation model msg
advance presentation =
    case presentation of
        Displaying prev active remaining ->
            case remaining of
                next :: future ->
                    TransitionForward prev active next future

                [] ->
                    presentation

        TransitionBack prev entering exiting remaining ->
            TransitionForward prev entering exiting remaining

        _ ->
            presentation


regress : Presentation model msg -> Presentation model msg
regress presentation =
    case presentation of
        Displaying prev active remaining ->
            case prev of
                [] ->
                    presentation

                last :: previous ->
                    TransitionBack previous last active remaining

        TransitionForward prev exiting entering remaining ->
            TransitionBack prev exiting entering remaining

        _ ->
            presentation


completeTransition : Presentation model msg -> Presentation model msg
completeTransition presentation =
    case presentation of
        TransitionForward prev exiting entering future ->
            Displaying (exiting :: prev) entering future

        TransitionBack prev entering exiting future ->
            Displaying prev entering (exiting :: future)

        _ ->
            presentation



-- View


view : (Msg -> msg) -> Presentation model msg -> model -> Html msg
view msgConvert presentation model =
    div
        [ style "display" "flex"
        , style "overflow" "hidden"
        ]
        (showPresentation msgConvert presentation model)


showPresentation : (Msg -> msg) -> Presentation model msg -> model -> List (Html msg)
showPresentation msgConvert presentation model =
    case presentation of
        Displaying previous active remaining ->
            List.map (hiddenSlide model) previous ++ List.map (visibleSlide model) (active :: remaining)

        TransitionForward previous exiting entering remaining ->
            List.map (hiddenSlide model) previous ++ [ leavingLeft msgConvert model exiting ] ++ List.map (visibleSlide model) (entering :: remaining)

        TransitionBack previous entering exiting remaining ->
            List.map (hiddenSlide model) previous ++ [ returningLeft msgConvert model entering ] ++ List.map (visibleSlide model) (exiting :: remaining)


visibleSlide : model -> Slide model msg -> Html msg
visibleSlide model slide =
    div
        (slideStyle [])
        [ toHtml slide model ]


hiddenSlide : model -> Slide model msg -> Html msg
hiddenSlide model slide =
    div
        (slideStyle
            [ ( "margin-left", "-100vw" )
            ]
        )
        [ toHtml slide model ]


transition : ( String, String )
transition =
    ( "transition", "0.5s" )


leavingLeft : (Msg -> msg) -> model -> Slide model msg -> Html msg
leavingLeft msgConvert model slide =
    div
        (slideStyle
            [ ( "margin-left", "-100vw" )
            , transition
            ]
            ++ [ on "transitionend" (Decode.succeed (msgConvert AnimationFinished)) ]
        )
        [ toHtml slide model ]


returningLeft : (Msg -> msg) -> model -> Slide model msg -> Html msg
returningLeft msgConvert model slide =
    div
        (slideStyle
            [ ( "margin-left", "0" )
            , transition
            ]
            ++ [ on "transitionend" (Decode.succeed (msgConvert AnimationFinished)) ]
        )
        [ toHtml slide model ]


slideStyle : List ( String, String ) -> List (Html.Attribute msg)
slideStyle attrs =
    [ style "width" "100vw"
    , style "height" "100vh"
    , style "flex-shrink" "0"
    ]
        ++ (attrs
                |> List.map
                    (\( prop, val ) ->
                        style prop val
                    )
           )



-- Subscriptions


subscriptions : Sub Msg
subscriptions =
    onKeyDown keyDecoder


keyDecoder : Decode.Decoder Msg
keyDecoder =
    Decode.map toDirection (Decode.field "key" Decode.string)


toDirection : String -> Msg
toDirection string =
    case string of
        "ArrowLeft" ->
            Previous

        "ArrowRight" ->
            Next

        _ ->
            NoOp
