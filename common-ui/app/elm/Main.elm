module Main exposing (main)

import Browser
import Component
import Content exposing (slides)
import Html exposing (Html, div)
import Html.Attributes exposing (class)
import Html.Events exposing (onClick)
import Presentation exposing (Presentation)
import Sim


main : Program () Model Msg
main =
    Browser.element
        { init = init
        , update = update
        , view = view
        , subscriptions = \_ -> Sub.map PresentAction Presentation.subscriptions
        }


type alias Model =
    { presentationState : Presentation Sim.Model Msg
    , simState : Sim.Model
    }


init : () -> ( Model, Cmd Msg )
init _ =
    let
        ( simModel, simCmd ) =
            Sim.init
    in
    ( { presentationState = Presentation.create (Content.slides SimMsg ComMsg)
      , simState = simModel
      }
    , Cmd.map SimMsg simCmd
    )


type Msg
    = PresentAction Presentation.Msg
    | SimMsg Sim.Msg
    | ComMsg Component.Msg


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        PresentAction presentMsg ->
            ( { model
                | presentationState = Presentation.update presentMsg model.presentationState
              }
            , Cmd.none
            )

        SimMsg simMsg ->
            let
                ( newModel, cmd ) =
                    Sim.update simMsg model.simState
            in
            ( { model | simState = newModel }, Cmd.map SimMsg cmd )

        ComMsg comMsg ->
            ( { model | simState = Component.update comMsg model.simState }, Cmd.none )


view : Model -> Html Msg
view model =
    div []
        [ Presentation.view PresentAction model.presentationState model.simState
        , div
            [ class "previous-control"
            , onClick (PresentAction Presentation.Previous)
            ]
            []
        , div
            [ class "next-control"
            , onClick (PresentAction Presentation.Next)
            ]
            []
        ]
