module Main exposing (main)

{-| The main module
@docs main
-}

import Slides exposing (..)
import Slides.Styles as Styles
import Html exposing (div, h1, a, text)
import Html.Attributes exposing (href, target)


{-| The program
-}
main : Program Never Model Msg
main =
    Slides.app
        options
        [ md
            """
        # What is Elm?

         - A programming language (surprise!)
         - Designed for building interactive applications
         - Designed for saftey and robustness
         - Designed for approachability & ease of learning
         - Part of the ML family of languages (Haskell, OCaml)
         - Compiles to Javascript
        """
        , md
            """
        # What isn't Elm?

        - A "better javascript"
        """
        , md
            """
        # What are the fundamental building blocks of a program?
            """
        , md
            """
        # Functions
        """
        , md
            """
        ### Stateless & Isolated

        ```elm
        add x y =
            x + y
        ```
        These don't work:
        ```elm
        add x y =
            x + y + externalMutableState
        ```

        ```elm
        add x y =
            deleteUser "joe"
            x + y
        ```
        """
        , md
            """
        ### Optional type annotations

        ```elm
        add : Int -> Int -> Int
        add x y
            = x + y
        ```

        ```elm
        first : Array containedType -> Maybe containedType
        first array =
            Array.get 0 array
        ```

        ```elm
        update : Msg -> Model -> (Model, Cmd Msg)
        update msg model =
            ...
        ```
        """
        , md
            """

        ### Currying

        It's like an automatic `Function.prototype.bind`

        ```elm
        add : Int -> Int -> Int
        add x y
            = x + y

        add5 : Int -> Int
        add5 = add 5

        -- add5 10 => 15
        ```
        """
        , md
            """
        # Records

        A little like type-checked objects in js
        """
        , md
            """
        ### Defining & Creating Records

        ```elm
        type alias Person =
            { name : String
            , age : Int
            }

        bob : Person
        bob =
            { name = "Bob Jones"
            , age = 30
            }
        ```
        """
        , md
            """
        ### Reading & Changing Records

        ```elm
        bobAge = bob.age

        bobAge = .age bob

        bobClone = { bob | age = 0 }
        ```
        """
        , md
            """
        # Custom Types

        Like enums with extra data
        """
        , md
            """
        ```elm
        type Bool
            = True
            | False

        type ContrivedIntegerParseResult
            = Success Int
            | Failure String

        type Result error value
            = Ok value
            | Err error

        type Maybe a
            = Just a
            | Nothing

        type RemoteData err val
            = NotAsked
            | Loading
            | Failure err
            | Success val
        ```
        """
        , md
            """
        ### Using Custom Types

        ```elm
        showParseResult : Result String Int -> Html msg
        showParseResult result =
            case result of

                Ok value ->
                    text ("Result was: " ++ (String.fromInt value))

                Err errMsg ->
                    text ("Unable to parse input: " ++ errMsg)
        ```
        """
        , md
            """
        ```elm
        userView : RemoteData Http.Error User -> Html msg
        userView userRequest =
            case userRequest of

                NotAsked ->
                    button [ onClick LoadUser ]
                        [ text "Click to load user" ]

                Loading ->
                    showSpinner

                Success user ->
                    userProfile user

                Failure error ->
                    div [ class "error" ]
                        [ div [] [ text "Error loading user!" ]
                        , div [] [ httpErrorDetails error ]
                        ]
        ```

        """
        , md
            """
        # The Elm Architecture (TEA)

        Stuff gets weird
        """
        , md
            """
        ### Only one(ish) way to structure an application in Elm

        ```elm
        main =
            Browser.element
                { init = init
                , update = update
                , view = view
                , subscriptions = subs
                }

        init : flags -> (model, Cmd msg)

        update : msg -> model -> (model, Cmd msg)

        view : model -> Html msg

        subs: model -> Sub msg
        ```

        `msg`, `model` are developer-defined types
        """
        , html
            (div []
                [ h1 [] [ text "Let's see it in action!" ]
                , div []
                    [ a [ href "simple-explainer.html", target "_blank" ]
                        [ text "Simple Version" ]
                    ]
                , div []
                    [ a [ href "giphy-explainer.html", target "_blank" ]
                        [ text "Complex Version" ]
                    ]
                ]
            )
        ]


options =
    slidesDefaultOptions
