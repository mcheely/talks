module.exports = {
  files: {
    javascripts: { joinTo: "main.js" },
    stylesheets: { joinTo: "style.css" }
  },
  plugins: {
    elm: {
      "exposed-modules": ["Main", "AnnotatedGif", "AnnotatedCounter"],
      renderErrors: true,
      parameters: ["--yes", "--warn"]
    }
  },
  overrides: {
    production: {
      plugins: {
        elm: {
          renderErrors: false,
          parameters: ["--yes", "--warn"]
        }
      }
    }
  }
};
